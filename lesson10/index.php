<?php
require_once 'classes/MainClass.php';
require_once 'classes/HotelRoom.php';
require_once 'classes/Appartment.php';
require_once 'classes/House.php';
require_once 'system_data/arrays.data.php';


foreach ($mainArray as $myArr) {

    switch ($myArr['type']) {
        case 'appartment':
            $immovablesObjects[] = new Appartment(
                $myArr['title'],
                $myArr['type'],
                $myArr['address'],
                $myArr['price'],
                $myArr['description'],
                $myArr['kitchen']
            );
            break;
        case 'house':
            $immovablesObjects[] = new House(
                $myArr['title'],
                $myArr['type'],
                $myArr['address'],
                $myArr['price'],
                $myArr['description'],
                $myArr['roomsAmount']
            );
            break;
        case 'hotel_room':
            $immovablesObjects[] = new HotelRoom(
                $myArr['title'],
                $myArr['type'],
                $myArr['address'],
                $myArr['price'],
                $myArr['description'],
                $myArr['roomNumber']
            );
            break;
        default:
            $immovablesObjects[] = new MainClass(
                $myArr['title'],
                $myArr['type'],
                $myArr['address'],
                $myArr['price'],
                $myArr['description']
            );
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lesson 10</title>
    <meta name="lesson 10" content="OOP">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-4">
                <?php foreach ($immovablesObjects as $key => $immovablesObject) : ?>
                    <ul>
                        <?= $immovablesObject->getSummaryLine() ?> <a href="./details.php?object_id=<?=(string)$key?>">Подробнее...</a>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>

</html>