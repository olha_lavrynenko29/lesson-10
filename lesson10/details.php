<?php
require_once 'classes/MainClass.php';
require_once 'classes/MainClassWriter.php';
require_once 'classes/HotelRoom.php';
require_once 'classes/Appartment.php';
require_once 'classes/House.php';
require_once 'system_data/arrays.data.php';

require_once 'classes/AppartmentWriter.php';
require_once 'classes/HouseWriter.php';
require_once 'classes/HotelRoomWriter.php';


if (!empty($_GET['object_id']) || $_GET['object_id'] == 0) {
    $objectId = $_GET['object_id'];
} else {
    header('Location:/');
    die();
}

$myArr = $mainArray[$objectId];
$writer = new MainClassWriter();
switch ($myArr['type']) {
    case 'appartment':
        $myObject = new Appartment(
            $myArr['title'],
            $myArr['type'],
            $myArr['address'],
            $myArr['price'],
            $myArr['description'],
            $myArr['kitchen']
        );
        $writer = new AppartmentWriter();
        break;
    case 'house':
        $myObject = new House(
            $myArr['title'],
            $myArr['type'],
            $myArr['address'],
            $myArr['price'],
            $myArr['description'],
            $myArr['roomsAmount']
        );
        $writer = new HouseWriter();
        break;
    case 'hotel_room':
        $myObject = new HotelRoom(
            $myArr['title'],
            $myArr['type'],
            $myArr['address'],
            $myArr['price'],
            $myArr['description'],
            $myArr['roomNumber']
        );
        $writer = new HotelRoomWriter();
        break;
    default:
        $myObject = new MainClass(
            $myArr['title'],
            $myArr['type'],
            $myArr['address'],
            $myArr['price'],
            $myArr['description']
        );
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?= $writer->write($myObject)['title'] ?>
</body>

</html>