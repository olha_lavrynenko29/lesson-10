<?php
class HotelRoom extends MainClass
{

    public $roomNumber = 0;

    public function __construct($title, $type, $address, $price, $description, $roomNumber)
    {
        parent::__construct($title, $type, $address, $price, $description);
        $this->roomNumber = $roomNumber;
    }

    public function getSummaryLine()
    {
        return parent::getSummaryLine()  . 'Номер номера:' . $this->roomNumber;
    }
}
