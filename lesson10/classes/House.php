<?php
class House extends MainClass
{

    public $roomsAmount = 0;

    public function __construct($title, $type, $address, $price, $description, $roomsAmount)
    {
        parent::__construct($title, $type, $address, $price, $description);
        $this->roomsAmount = $roomsAmount;
    }

    public function getSummaryLine()
    {
        return parent::getSummaryLine()  . ' Количество комнат:' . $this->roomsAmount;
    }
}
