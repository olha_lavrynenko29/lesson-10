<?php
class MainClass
{
    public $title = '';
    public $type = '';
    public $address = '';
    public $price = 0.0;
    public $description = '';

    public function __construct($title, $type, $address, $price, $description)
    {
        $this->title = $title;
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
    }

    public function getSummaryLine()
    {
        return '<strong>' . $this->title . '</strong>  - Тип: ' . $this->type . ', ' . '<strong>' . $this->address .  '</strong>  - Цена: ' . $this->price . ',';
    }
}
